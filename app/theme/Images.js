const Images = {
    leftIconMenu: require('../img/leftMenuIcon.png'),
    bgLogin: require('../img/bgLogin.jpg'),
    logoSumarInversion: require('../img/logoSumarInversion.png'),
    logoSumarInversionCirlce: require('../img/logo_sumarinversion_circle.png'),
    bgIntro: require('../img/introBg.png'),
    cerrarSesionIcon: require('../img/cerrarSesionIcon.png'),
    misInversionesIcon: require('../img/misInversionesIcon.png'),
    proyectosRecomendadosIcon: require('../img/proyectosRecomendadosIcon.png'),
    contactoIcon: require('../img/contactoIcon.png'),
}

export { Images };