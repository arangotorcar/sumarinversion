import { StyleSheet } from 'react-native';
import { Colors } from '../theme/Colors';
import { Dimensions } from 'react-native';

const styles = StyleSheet.create({
    //general
    mainContainer: {
        flex: 1,
    },
    bodyContent: {
        padding: 15,
        flex: 1,
    },
    textInputHolder: {
        
    },
    title: {
        fontSize: 24,
        color: Colors.colorA,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    bgLogin: {
        width: '100%',
        height: '100%',
    },
    overlight: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,.6)'
    },
    textInputContainerStyle: {
        backgroundColor: Colors.blanco,
        paddingLeft: 15,
        borderRadius: 8,
        marginBottom: 15,
    },
    bordeGris: {
        flex: 1,
        height: 1,
        backgroundColor: Colors.colorE,
    },
    customFontMoonFlower: {
        fontFamily: 'Moon Flower',
        fontSize: 28
    },
    customFontDDDINBold: {
        fontFamily: 'D-DIN Bold',
    },
    customFontDDDIN: {
        fontFamily: 'D-DIN',
    },
    borderer1: {
        borderWidth: .5,
        borderColor: Colors.grisClaro,
        marginBottom: 20,
        backgroundColor: Colors.grisClaro
    },
    line: {
        flex: 1,
        height: 1,
        backgroundColor: Colors.grisClaro
    },

    //leftmenu
    leftMenuholder: {
        flex: 1,
        backgroundColor: Colors.blanco
    },
    imageLeftMenuHolder: {
        height: 160,
        backgroundColor: Colors.colorB,
    },
    imgLeftMenu: {
        height: undefined,
        width: undefined,
        flex: 1
    },
    imgLeftMenuOverlay: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        backgroundColor: `rgba(${Colors.colorARGB},.5)`,
        alignItems: 'center',
        justifyContent: 'center'
    },
    listItemLeftMenu: {
        flex: 1,
    },
    menuItemHolder: {
        flex: 1,
    },
    menuItem: {
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        padding: 12,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 1
    },
    iconLeftMenu: {
        
    },
    titleSection: {
        flex: 1,
        fontSize: 18,
        marginLeft: 10,
        color: Colors.negro
    },

    //loginscreen
    logoLogin: {
        width: 300,
        height: 100,
        alignSelf: 'center',
        resizeMode: 'contain',
        marginBottom: 30
    },

    //mis inversiones screen
    itemHolder: {
        flexDirection: 'row',
        width: Dimensions.get('window').width
    },
    imageHolder: {
        width: 180,
        height: 210,
    },
    titleHolderMI: {
        backgroundColor: Colors.colorC,
        alignItems: 'center',
        paddingVertical: 5,
    },  
    imagen: {
        height: undefined,
        width: undefined,
        flex: 1
    },
    txtModules: {
        position: 'absolute',
        bottom: 0,
        top: 0,
        right: 0,
        left: 0,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    txtModulesHolder: {
        borderRadius: 12,
        padding: 8,
        backgroundColor: Colors.colorC,
        marginBottom: 15
    },
    tituloHolder: {
        flex: 1,
        justifyContent: 'center',
        padding: 10,
        alignItems: 'center'
    },
    titulo: {
        fontSize: 16,
        fontWeight: 'bold', 
        color: Colors.colorC    
    },
    parrafo: {
        marginTop: 10,
    },
    
    infoGlobalHolderMI: {
        alignItems: 'center'
    },  
    tituloGlobal: {
        fontWeight: 'bold',
        fontSize: 16,
        marginVertical: 5
    },
    itemGlobal: {
        marginVertical: 15
    },
    cantidadGlobal: {
        fontSize: 20
    },

    //proyetos destacados
    itemHolderDestacado: {
        marginBottom: 10
    },
    imageHolderDestacado: {
        height: 200
    },  
    imagenDestacado: {
        height: undefined,
        width: undefined,
        flex: 1
    },
    tituloHolderDestacado: {
        flex: 1,
        justifyContent: 'center',
        padding: 10,
        alignItems: 'center'
    },
    tituloDestacado: {
        fontSize: 16,
        fontWeight: 'bold', 
        color: Colors.colorC    
    },
    parrafoDestacado: {
        marginTop: 10,
    },
    progresoLabelDestacado: {
        marginVertical: 5,
        fontWeight: 'bold'
    },
    progressHolderDestacado: {
        marginVertical: 20,
    },

    //detalle proyecto
    imageDetailHolder: {
        height: 200,
        position: 'relative'
    },
    imageDetail: {
        width: undefined,
        height: undefined,
        flex: 1,
        resizeMode: 'cover'
    },
    parrafoDetail: {
        
    },
    dataProyectHolder: {
        marginTop: 15
    },
    dato:{
        marginVertical: 5,
    },
    boldTxt: {
        fontWeight: 'bold',
    },
    progressHolder: {
        alignItems: 'center',
        marginVertical: 5,
    }
});

export default styles;