const Colors = {
    //dep orange
    colorA: '#aa3815',
    colorB: '#222222',
    colorC: '#376d91',
    colorD: '#852508',
    colorE: '#494949',
    colorF: '#cb5e3c',
    colorARGB: '73, 73, 73',
    blanco: '#fff',
    negro: '#000',
    verde: '#74C97B',
    grisClaro: '#eee',
    grisA: '#dddddd',
    gris: '#494949',
    dark: '#222222'
}

export { Colors };