import React, { Component } from 'react-native';
import { Text, TextInput } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { registerScreens } from './navigation';
import { Colors } from './theme/Colors';
import { TopBarButtons } from './navigation/TopBarButtons';
import { Storage } from './utils/Storage';
import { setCustomText } from 'react-native-global-props';

const customTextProps = {
  style: {
    color: Colors.dark,
    fontFamily: 'D-DIN',
  }
};

setCustomText(customTextProps);

if (Text.defaultProps == null) Text.defaultProps = {};
Text.defaultProps.allowFontScaling = false;

if (TextInput.defaultProps == null) TextInput.defaultProps = {};
TextInput.defaultProps.allowFontScaling = false;

registerScreens();

Navigation.events().registerNavigationButtonPressedListener(({ buttonId }) => {
  console.log(buttonId);
  switch (buttonId) {
    case TopBarButtons.LEFT_MENU_ICON:
    Navigation.mergeOptions(TopBarButtons.ID_LEFT_MENU, {
      sideMenu: {
        left: {
          visible: true
        }
      }
    });
    break;
  }    
});

Navigation.events().registerAppLaunchedListener(() => {
  
  Storage.get('userId', (userId)=>{
    if(userId === undefined || userId === null || userId === ''){
      Navigation.setDefaultOptions({
        layout: {
          orientation: ['portrait'],
          backgroundColor: 'white'
        },
        topBar: {
          backButton: {
            title: '',
            color: 'white'
          },
          background: {
            color: Colors.colorE
          },
          drawBehind: false,
          animate: true,
          title: {
            color: 'white',
            fontFamily: 'Moon Flower',
            fontSize: 28,
            alignment: 'center',
          }
        },
      });
      
      Navigation.setRoot({
        root: {
          component: {
            id: 'IntroScreen',
            name: 'IntroScreen',
            options: {
              topBar: {
                visible: false,
                drawBehind: true,
                animate: false
              }
            }
          }
        }
      });
    }else{
      Navigation.setDefaultOptions({
        layout: {
          orientation: ['portrait'],
          backgroundColor: 'white'
        },
        topBar: {
          backButton: {
            title: '',
            color: 'white'
          },
          background: {
            color: Colors.colorE
          },
          drawBehind: false,
          animate: true,
          title: {
            color: 'white',
            fontFamily: 'Moon Flower',
            fontSize: 28,
            alignment: 'center',
          }
        },
      });
      
      Navigation.setRoot({
        root: {
          sideMenu: {
            left: {
              component: {
                id: TopBarButtons.ID_LEFT_MENU,
                name: 'LeftMenu',
              }
            },
            center: {
              stack: {
                id: 'AppStack',
                children: [
                  {
                    component: {
                      id: "MisInversionesScreen",
                      name: "MisInversionesScreen",
                    },
                    options: {
                      topBar: {
                        title: {
                          fontFamily: 'Moon Flower',
                          fontSize: 28,
                          alignment: 'center'
                        }
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      });
    }
  });
});

//No borrar
//JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_181.jdk/Contents/Home
//export JAVA_HOME="/Applications/Android Studio.app/Contents/jre/jdk/Contents/Home"
//No borrar
