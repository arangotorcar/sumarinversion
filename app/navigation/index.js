import { Navigation } from 'react-native-navigation';
import MisInversionesScreen from '../screens/MisInversionesScreen';
import LoginScreen from '../screens/LoginScreen';
import LeftMenu from '../screens/LeftMenu';
import ProyectosRecomendadosScreen from '../screens/ProyectosRecomendadosScreen';
import DetalleProyectoScreen from '../screens/DetalleProyectoScreen';
import WebViewScreen from '../screens/WebViewScreen';
import IntroScreen from '../screens/IntroScreen';
import WebViewGeneralScreen from '../screens/WebViewGeneralScreen';

export function registerScreens() {
    Navigation.registerComponent('MainScreen', ()=>MainScreen);
    Navigation.registerComponent('LeftMenu', ()=>LeftMenu);
    Navigation.registerComponent('LoginScreen', ()=>LoginScreen);
    Navigation.registerComponent('MisInversionesScreen', ()=>MisInversionesScreen);
    Navigation.registerComponent('ProyectosRecomendadosScreen', ()=>ProyectosRecomendadosScreen);
    Navigation.registerComponent('DetalleProyectoScreen', ()=>DetalleProyectoScreen);
    Navigation.registerComponent('WebViewScreen', ()=>WebViewScreen);
    Navigation.registerComponent('IntroScreen', ()=>IntroScreen);
    Navigation.registerComponent('WebViewGeneralScreen', ()=>WebViewGeneralScreen);
};