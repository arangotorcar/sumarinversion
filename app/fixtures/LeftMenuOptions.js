import { Images } from "../theme/Images";

const LeftMenuOptions = [
    {
        titulo: 'Mis Inversiones',
        screen: 'MisInversionesScreen',
        icono: Images.misInversionesIcon,
    },
    {
        titulo: 'Proyectos Recomendados',
        screen: 'ProyectosRecomendadosScreen',
        icono: Images.proyectosRecomendadosIcon,
    },
    {
        titulo: 'Contacto',
        screen: 'WebViewGeneralScreen',
        icono: Images.contactoIcon,
    },
    {
        titulo: 'Cerrar sesión',
        screen: 'CerrarSesion',
        icono: Images.cerrarSesionIcon,
    },
];

export default LeftMenuOptions;