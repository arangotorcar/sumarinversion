import React, { Component } from 'react';
import { View, Text, SafeAreaView } from 'react-native';
import { WebView } from "react-native-webview";
import styles from '../theme/styles';
import { TopBarButtons } from '../navigation/TopBarButtons';
import { Images } from '../theme/Images';
import { Navigation } from 'react-native-navigation';

export default class WebViewGeneralScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        url: this.props.url
    };
    Navigation.mergeOptions(this.props.componentId, {
        topBar: {
          leftButtons: [
            {
              id: TopBarButtons.LEFT_MENU_ICON,
              icon: Images.leftIconMenu,
              color: 'white',
            }
          ]
        },
    });
  }

  onMessage(e){
    console.log(e);
  }

  render() {
      const { url } = this.state;
      const jsInjected = "document.getElementById('sp-header').style.display = 'none'; document.getElementById('sp-footer').style.display = 'none'; document.getElementById('data-contact-holder').style.display = 'none'; document.getElementsByClassName('sp-scroll-top').style.display = 'none !important'; document.getElementsByClassName('sp-scroll-top').style.visible = 'false !important';";

    return (
        <SafeAreaView style={styles.mainContainer}>
            <WebView onError={(error)=>{console.log(error)}} onMessage={(e)=>{this.onMessage(e)}} originWhitelist={['*']} source={{uri: url}} startInLoadingState={true} javaScriptEnabled={true} injectedJavaScript={jsInjected}></WebView>
        </SafeAreaView>
    );
  }
}
