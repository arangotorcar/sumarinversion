import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import { Colors } from '../theme/Colors';
import { Images } from '../theme/Images';
import StylesG from '../theme/styles';
import { Navigation } from 'react-native-navigation';
import SplashScreen from 'react-native-splash-screen';

export default class IntroScreenSI extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  GoToLogin(){
    Navigation.setRoot({
        root: {
          component: {
            id: 'LoginScreen',
            name: 'LoginScreen',
            options: {
              topBar: {
                visible: false,
                drawBehind: true,
                animate: false
              }
            }
          }
        }
      });
  }

  render() {
    return (
      <ImageBackground style={styles.mainHolder} source={Images.bgIntro}>
            <View style={styles.logoHolder}>
                <Image style={styles.iconHeader} source={Images.logoSumarInversion}></Image>
            </View>
            <View>
                <View style={styles.txtIntroHolder}>
                    <Text style={[styles.textIntro]}>Binvenido a tu</Text>
                    <Text style={[styles.textIntroBold]}>Administrador de Inversiones</Text>
                </View>
                <TouchableWithoutFeedback onPress={()=>{this.GoToLogin()}}>
                    <View style={styles.btnIngresoHolder}>
                        <Text style={[styles.btnIngreso, StylesG.customFontMoonFlower, {fontSize: 34,}]}>{'ingresar >'.toUpperCase()}</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
      </ImageBackground>
    );
  }

  componentDidMount() {
    SplashScreen.hide();
  }

}

const styles = StyleSheet.create(
    {
        mainHolder: {
            justifyContent: 'space-between',
            flex: 1
        },
        logoHolder: {
            backgroundColor: Colors.negro,
            padding: 10,
        },
        iconHeader: {
            width: 220,
            height: 60,
            resizeMode: 'contain',
            alignSelf: 'center',
        },
        txtIntroHolder:{
            alignSelf: 'center',
            paddingVertical: 50,
        },
        textIntro: {
            fontFamily: 'D-DIN',
            color: Colors.blanco,
            fontSize: 22
        },
        textIntroBold: {
            fontFamily: 'D-DIN Bold',
            color: Colors.blanco,
            fontSize: 26
        },
        btnIngresoHolder: {
            backgroundColor: Colors.gris,
            padding: 10,
        },
        btnIngreso: {
            color: Colors.blanco,
            alignSelf: 'flex-end'
        }
    }
);
