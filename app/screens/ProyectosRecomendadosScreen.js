import React, { Component } from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableWithoutFeedback, Image, Alert } from 'react-native';
import styles from '../theme/styles';
import { Navigation } from 'react-native-navigation';
import { TopBarButtons } from '../navigation/TopBarButtons';
import { Images } from '../theme/Images';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import { Colors } from '../theme/Colors';
import { Button } from 'react-native-elements';
import { WaveIndicator } from 'react-native-indicators';
import { SumarInversionApi } from '../services/index';
import HTMLView from 'react-native-htmlview';

const proyectos = [
  {
    Titulo: 'Casa de Diseño Z02 Pilar, Buenos Aires',
    Descripcion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pretium orci. Integer nec cursus leo, vel sodales felis.',
    Imagen: 'https://placeimg.com/1280/720/arch',
  },
  {
    Titulo: 'Casa de Diseño Z02 Pilar, Buenos Aires',
    Descripcion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pretium orci. Integer nec cursus leo, vel sodales felis.',
    Imagen: 'https://placeimg.com/1280/720/arch',
  },
  {
    Titulo: 'Casa de Diseño Z02 Pilar, Buenos Aires',
    Descripcion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pretium orci. Integer nec cursus leo, vel sodales felis.',
    Imagen: 'https://placeimg.com/1280/720/arch',
  },
  {
    Titulo: 'Casa de Diseño Z02 Pilar, Buenos Aires',
    Descripcion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pretium orci. Integer nec cursus leo, vel sodales felis.',
    Imagen: 'https://placeimg.com/1280/720/arch',
  }
];

export default class ProyectosRecomendadosScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      proyectosDestacados: []
    };

    Navigation.mergeOptions(this.props.componentId,  {
      topBar: {
        leftButtons: {
          id: TopBarButtons.LEFT_MENU_ICON,
          icon: Images.leftIconMenu,
          color: 'white',
        }
      }
    });
  }

  navToDetailFeature(item){
    Navigation.push(this.props.componentId, {
      component: {
        id: 'WebViewScreen',
        name: 'WebViewScreen',
        passProps: {
          idProyecto: item.idProyecto
        },
        options: {
          topBar: {
            title: {
              text: item.titulo
            }
          }
        }
      }
    });
  }

  calculatePercent(item){
    console.log('***** calcular porcentaje ******');
    console.log(item);
    const cantidadModulos = parseInt(item.cantidadModulos);
    const percent = (item.modulosVendidos*100)/cantidadModulos;
    console.log('***** porcentaje obtenido ******');
    console.log(percent);
    return percent.toFixed(0);
  }

  render() {
    const { loading, proyectosDestacados } = this.state;
    return (
      <SafeAreaView style={styles.mainContainer}>
        {
          loading ? <WaveIndicator count={1} waveFactor={0.1} color={'rgb(55,109,145)'} size={60}></WaveIndicator> :
          proyectosDestacados.length === 0 ? <Text style={{textAlign: 'center', marginTop: 20}}>Por el momento no hay proyectos destacados.</Text> :
          <FlatList data={proyectosDestacados} renderItem={({item})=>
            <TouchableWithoutFeedback onPress={()=>{this.navToDetailFeature(item)}}>
              <View>  
                <View style={styles.itemHolderDestacado}>
                  <View style={styles.imageHolderDestacado}>
                    <Image resizeMethod={'resize'} style={styles.imagenDestacado} source={{uri: item.image}} ></Image>
                  </View>
                  <View style={styles.tituloHolderDestacado}>
                    <Text style={[styles.tituloDestacado]}>{item.titulo}</Text>
                    <Text style={styles.parrafoDestacado}>{item.ubicacion}</Text>
                    <HTMLView style={{marginVertical: 15}} value={item.detalle}></HTMLView>
                    <View style={styles.progressHolderDestacado}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5}}>
                          <Text style={styles.progresoLabel}>Financiamiento:</Text>
                          <Text style={styles.progresoLabel}>{this.calculatePercent(item)}%</Text>
                        </View>
                        <ProgressBarAnimated borderWidth={0} borderRadius={0} height={5} backgroundColor={Colors.dark} width={250} value={this.calculatePercent(item)}/>
                    </View>
                    <Button onPress={()=>{this.navToDetailFeature(item)}} title={'+ info'} borderRadius={30} backgroundColor={Colors.colorC} style={{marginVertical: 15, height: 40, width: 80, alignSelf: 'center'}}></Button>
                  </View>
                </View>
                <View style={styles.bordeGris}></View>
              </View>
            </TouchableWithoutFeedback>
          } keyExtractor={(item, key)=>key}>
          </FlatList>
        }
      </SafeAreaView>
    );
  }

  componentDidMount(){
    SumarInversionApi.GetProyectosDestacados().then((res)=>{
      console.log('********+ respuesta proyectos recomendados *********** ');
      console.log(res);
      if (res.problem === 'NETWORK_ERROR') {
        this.setState({ loading: false });
        Alert.alert('', 'Al parecer existe un problema con tu conexión, intenta más tarde', [{
          text: 'Aceptar',
        }]);
        return;
      }
      if (res.problem === 'CLIENT_ERROR') {
        this.setState({ loading: false });
        Alert.alert('', 'No es posible enviar la solicitud, intenta más tarde', [{
          text: 'Aceptar',
        }]);
        return;
      }
      if (res.problem === 'TIMEOUT_ERROR') {
        this.setState({ loading: false });
        Alert.alert('', 'El servicio tardo demasiado en responder, intenta más tarde', [{
          text: 'Aceptar',
        }]);
        return;
      }
      if (res.problem === 'SERVER_ERROR') {
        this.setState({ loading: false });
        Alert.alert('', 'No se pudo procesar la solicitud, intenta más tarde', [{
          text: 'Aceptar',
        }]);
        return;
      }
      if (res.problem === 'CONNECTION_ERROR') {
        this.setState({ loading: false });
        Alert.alert('', 'Servidor no disponible, intenta más tarde', [{
          text: 'Aceptar',
        }]);
        return;
      }else{
        this.setState({
          proyectosDestacados: res.data.proyectosDestacados,
          loading: false
        });
      }
    }).catch((error)=>{
      Alert.alert('', error, [
        {
          text: 'Aceptar'
        }
      ], {
        cancelable: false
      });
    });
  }
}
