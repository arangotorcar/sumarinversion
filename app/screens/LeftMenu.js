import React, { Component } from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableOpacity, Image } from 'react-native';
import styles from '../theme/styles';
import LeftMenuOptions from '../fixtures/LeftMenuOptions';
import { Navigation } from 'react-native-navigation';
import { Icon } from 'react-native-elements'
import { Colors } from '../theme/Colors';
import { TopBarButtons } from '../navigation/TopBarButtons';
import { Images } from '../theme/Images';
import { Storage } from '../utils/Storage';

export default class LeftMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  navTo(section){
    if(section.titulo === 'Contacto'){
      Navigation.setStackRoot('AppStack', {
        component: {
          name: section.screen,
          passProps: {
            url: 'https://sumarinversion.com.ar/contacto'
          },
          options: {
            topBar: {
              drawBehind: false,
              animate: true,
              title: {
                text: section.titulo
              }
            }
          }
        }
      });
      Navigation.mergeOptions(TopBarButtons.ID_LEFT_MENU, {
        sideMenu: {
          left: {
            visible: false
          }
        }
      });
    }else{
      if(section.screen === 'CerrarSesion'){
        Storage.set('nombre', '', ()=>{
          Storage.set('apellido', '', ()=>{
            Storage.set('email', '', ()=>{
              Storage.set('userId', '', ()=>{
                Navigation.setRoot({
                  root: {
                    component: {
                      id: 'LoginScreen',
                      name: 'LoginScreen',
                      options: {
                        topBar: {
                          visible: false,
                          drawBehind: false,
                          animate: true
                        }
                      }
                    }
                  }
                });
              })
            });
          });
        });
      }else{
        Navigation.setStackRoot('AppStack', {
          component: {
            name: section.screen,
            options: {
              topBar: {
                drawBehind: false,
                animate: true,
                title: {
                  text: section.titulo
                }
              }
            }
          }
        });
        Navigation.mergeOptions(TopBarButtons.ID_LEFT_MENU, {
          sideMenu: {
            left: {
              visible: false
            }
          }
        });
      }
    }
  }

  render() {
    return (
      <View style={styles.leftMenuholder}>
        <View style={styles.imageLeftMenuHolder}>
          <Image style={styles.imgLeftMenu} source={{uri: 'https://sumarinversion.com.ar/assets/images/slide1.jpg'}}></Image>
          <View style={styles.imgLeftMenuOverlay}>
            <Image style={{width: 90, height: 90, opacity: .3}} source={Images.logoSumarInversionCirlce}></Image>
          </View>
        </View>
        <FlatList style={styles.listItemLeftMenu} data={LeftMenuOptions} renderItem={({item})=>
          <TouchableOpacity style={styles.menuItemHolder} onPress={()=>{this.navTo(item)}}>
            <View style={styles.menuItem}>
              {/* <Icon style={styles.iconLeftMenu} name={item.icono} type={'material-community'} size={25} color={Colors.colorC}></Icon> */}
              <Image style={{width: 45, height: 45, resizeMode: 'contain'}} source={item.icono}></Image>
              <Text style={styles.titleSection}>{item.titulo}</Text>
            </View>
          </TouchableOpacity>
        } keyExtractor={(item, key)=>item.titulo}>
        </FlatList>
      </View>
    );
  }
}
