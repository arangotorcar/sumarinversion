import React, { Component } from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableWithoutFeedback, Image, Alert, ScrollView } from 'react-native';
import styles from '../theme/styles';
import { Navigation } from 'react-native-navigation';
import { Images } from '../theme/Images';
import { TopBarButtons } from '../navigation/TopBarButtons';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import { Colors } from '../theme/Colors';
import { WaveIndicator } from 'react-native-indicators';
import { SumarInversionApi } from '../services/index';
import { Storage } from '../utils/Storage'; 
import moment from 'moment';
import currencyFormatter from 'currency-formatter';
import SplashScreen from 'react-native-splash-screen';

const proyectos = [
  {
    Titulo: 'Casa de Diseño Z02 Pilar, Buenos Aires',
    Descripcion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pretium orci. Integer nec cursus leo, vel sodales felis.',
    Imagen: 'https://placeimg.com/1280/720/arch',
  },
  {
    Titulo: 'Casa de Diseño Z02 Pilar, Buenos Aires',
    Descripcion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pretium orci. Integer nec cursus leo, vel sodales felis.',
    Imagen: 'https://placeimg.com/1280/720/arch',
  },
  {
    Titulo: 'Casa de Diseño Z02 Pilar, Buenos Aires',
    Descripcion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pretium orci. Integer nec cursus leo, vel sodales felis.',
    Imagen: 'https://placeimg.com/1280/720/arch',
  },
  {
    Titulo: 'Casa de Diseño Z02 Pilar, Buenos Aires',
    Descripcion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pretium orci. Integer nec cursus leo, vel sodales felis.',
    Imagen: 'https://placeimg.com/1280/720/arch',
  }
];

export default class MisInversionesScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      misInversiones: [],
    };
    Navigation.mergeOptions(this.props.componentId, {
      topBar: {
        leftButtons: [
          {
            id: TopBarButtons.LEFT_MENU_ICON,
            icon: Images.leftIconMenu,
            color: 'white',
          }
        ],
        title: {
          text: 'Mis Inversones'
        }
      },
    });
  }

  navToDetail(item, porcentajeProyecto){
    Navigation.push(this.props.componentId, {
      component: {
        id: 'DetalleProyectoScreen',
        name: 'DetalleProyectoScreen',
        passProps: {
          proyecto: item,
          porcentajeProyecto
        },
        options: {
          topBar: {
            title: {
              text: item.titulo,
            }
          }
        }
      }
    });
  }

  calculateProgress(item){
    console.log('******* calculo progreso *******');
    console.log(item);
    const date1 = item.fecha;
    const today = new Date();
    const date2 = moment(today).format('YYYY-MM-DD');
    let diff = Math.abs(Date.parse(date2) - Date.parse(date1)).toString().substr(0, 8);
    console.log(diff);
    const years = Math.floor(diff / (365*60*60*24));
    const months = Math.floor((diff - years * 365*60*60*24) / (30*60*60*24));
    const days = Math.floor((diff - years * 365*60*60*24 - months*30*60*60*24)/ (60*60*24));
    const proyectoPorcentaje = Math.round((((years*12) + months)/item.plazo)*100);
    console.log('******** porcentaje obtenido ********');
    console.log(proyectoPorcentaje);
    return proyectoPorcentaje
  }

  CalculateTRPM(misInversiones){
    
  } 

  render() {
    const { loading, misInversiones } = this.state;
    let totalInvertido = 0;
    return (
      <ScrollView style={styles.mainContainer}>
        {
          loading ? <WaveIndicator count={1} waveFactor={0.1} color={'rgb(55,109,145)'} size={60}></WaveIndicator> : 
          misInversiones.length === 0 ? <Text style={{textAlign: 'center', marginTop: 20}}>Por el momento no tienes inversiones.</Text> :
          <View>
            <FlatList style={{flex: 1}} data={misInversiones} renderItem={({item})=>
              <TouchableWithoutFeedback underlayColor={'white'} onPress={()=>{this.navToDetail(item, this.calculateProgress(item))}}>
                <View>
                  <View style={styles.itemHolder}>
                    <View style={styles.imageHolder}>
                      <View style={styles.titleHolderMI}>
                        <Text style={[styles.customFontMoonFlower, {color: Colors.blanco, fontSize: 24}]}>{item.titulo}</Text>
                      </View>
                      <Image style={styles.imagen} source={{uri: item.image}} ></Image>
                      <View style={styles.txtModules}>
                        <View style={styles.txtModulesHolder}>
                          <Text style={{color: Colors.blanco, fontSize: 12}}>Modulos adquiridos: {item.modulosAdquiridos.length}</Text>
                        </View>
                      </View>
                    </View>
                    <View style={styles.tituloHolder}>
                      <Text style={[styles.titulo]}>{item.Titulo}</Text>
                      <View style={styles.progressHolder}>
                          <Text style={[styles.customFontDDDINBold, {marginBottom: 10}]}>Progreso del proyecto:</Text>
                          <Text style={[{marginBottom: 10, color: Colors.gris, fontSize: 18}]}>{this.calculateProgress(item)}%</Text>
                          <ProgressBarAnimated borderWidth={0} borderRadius={0} height={5} backgroundColor={Colors.dark} width={160} value={this.calculateProgress(item)}/>
                      </View>
                      <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 10}}>
                        <Text style={[styles.customFontDDDINBold]}>Inicio: </Text>
                        <Text>{moment(item.fecha).format('DD, YYYY')}</Text>
                      </View>
                      <View style={{flexDirection: 'row', justifyContent: 'center',}}>
                        <Text style={styles.customFontDDDINBold}>Plazo:</Text>
                        <Text> {item.plazo} meses</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.bordeGris}></View>
                </View>
              </TouchableWithoutFeedback>
            } keyExtractor={(item, key)=>key}>
            </FlatList>
            {/* <View style={styles.infoGlobalHolderMI}>
                <View style={styles.itemGlobal}>
                  <Text style={styles.tituloGlobal}>Total invertido</Text>
                  {                
                    misInversiones.map((item)=>{                  
                      item.modulosAdquiridos.map((i)=>{
                        totalInvertido += parseInt(i.inversion);
                      })
                    })
                  }
                  <Text style={styles.cantidadGlobal}>USD {currencyFormatter.format(totalInvertido, {code: 'USD', symbol: '', thousand: '.', decimal:',', precision: 0})}</Text>
                </View>
                <View>
                  <Text style={styles.tituloGlobal}>Total con Renta Proyectada Mínima</Text>
                  {
                    this.CalculateTRPM(misInversiones)
                  }
                </View>
            </View> */}
          </View>
        }
      </ScrollView>
    );
  }

  componentDidMount(){
    SplashScreen.hide();
    Storage.get('userId', (userId)=>{
      SumarInversionApi.GetMisInversiones(userId).then((res)=>{
        console.log('********+ respuesta mis inversiones *********** ');
        console.log(res);
        if (res.problem === 'NETWORK_ERROR') {
          this.setState({ loading: false });
          Alert.alert('', 'Al parecer existe un problema con tu conexión, intenta más tarde', [{
            text: 'Aceptar',
          }]);
          return;
        }
        if (res.problem === 'CLIENT_ERROR') {
          this.setState({ loading: false });
          Alert.alert('', 'No es posible enviar la solicitud, intenta más tarde', [{
            text: 'Aceptar',
          }]);
          return;
        }
        if (res.problem === 'TIMEOUT_ERROR') {
          this.setState({ loading: false });
          Alert.alert('', 'El servicio tardo demasiado en responder, intenta más tarde', [{
            text: 'Aceptar',
          }]);
          return;
        }
        if (res.problem === 'SERVER_ERROR') {
          this.setState({ loading: false });
          Alert.alert('', 'No se pudo procesar la solicitud, intenta más tarde', [{
            text: 'Aceptar',
          }]);
          return;
        }
        if (res.problem === 'CONNECTION_ERROR') {
          this.setState({ loading: false });
          Alert.alert('', 'Servidor no disponible, intenta más tarde', [{
            text: 'Aceptar',
          }]);
          return;
        }else{
          this.setState({
            misInversiones: res.data.misInversiones,
            loading: false
          });
        }
      }).catch((error)=>{
        Alert.alert('', error, [
          {
            text: 'Aceptar'
          }
        ], {
          cancelable: false
        });
      });
    });
  }

}
