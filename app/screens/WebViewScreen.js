import React, { Component } from 'react';
import { View, Text, SafeAreaView } from 'react-native';
import styles from '../theme/styles';
import { WebView } from "react-native-webview";

export default class WebViewScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      idProyecto: this.props.idProyecto
    };
  }

  onMessage(e){
    console.log(e);
  }

  render() {
    const { idProyecto } = this.state;
    const jsInjected = "document.getElementById('sp-header').style.display = 'none'; document.getElementById('sp-footer').style.display = 'none'; document.getElementsByClassName('sp-scroll-top').style.display = 'none !important'; document.getElementsByClassName('sp-scroll-top').style.visible = 'false !important';";

    return (
      <SafeAreaView style={styles.mainContainer}>
        <WebView onError={(error)=>{console.log(error)}} onMessage={(e)=>{this.onMessage(e)}} javaScriptEnabled={true} originWhitelist={['*']} injectedJavaScript={jsInjected} source={{uri: `http://www.sumarinversion.com.ar/proyecto?id=${idProyecto}`}} startInLoadingState={true}></WebView>
      </SafeAreaView>
    );
  }
}
