import React, { Component } from 'react';
import { View, SafeAreaView, ImageBackground, Image, Alert } from 'react-native';
import styles from '../theme/styles';
import { Button } from 'react-native-elements';
import TextInput from '../components/TextInput';
import { Colors } from '../theme/Colors';
import { Images } from '../theme/Images';
import { Navigation } from 'react-native-navigation';
import { TopBarButtons } from '../navigation/TopBarButtons';
import { SumarInversionApi } from '../services/index';
import { hex_md5 } from 'react-native-md5';
import { WaveIndicator } from 'react-native-indicators';
import Helpers from '../utils/Helpers';
import { Storage } from '../utils/Storage';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // email: 'leandroricoalcazar@gmail.com',
      // password: '30593289',
      // email: 'pablocrimi@gmail.com',
      // password: '35d240e7cf0853dd7df7d3acd645981e',
      // email: 'victor@estudiozabala.com',
      // password: '178391e75e588dfae64c9cb15a72492c', //25769870
      email: '',
      password: '',
      loading: false
    };
  }

  navToHome() {
    Navigation.setRoot({
      root: {
        sideMenu: {
          left: {
            component: {
              id: TopBarButtons.ID_LEFT_MENU,
              name: 'LeftMenu',
            }
          },
          center: {
            stack: {
              id: 'AppStack',
              children: [
                {
                  component: {
                    id: "MisInversionesScreen",
                    name: "MisInversionesScreen",
                  }
                }
              ]
            }
          }
        }
      }
    });
  }

  LoginExecute() {
    const { email, password } = this.state;
    if(email === '' || email === null || email === undefined){
      Alert.alert('', 'Debes ingresar tu email.', [{
        text: 'Aceptar',
      }]);
      return;
    }
    if(!Helpers.IsEmailValid(email)){
      Alert.alert('', 'Debes ingresar tu email con el formato correcto.', [{
        text: 'Aceptar',
      }]);
      return;
    }
    if(password === '' || password === null || password === undefined){
      Alert.alert('', 'Debes ingresar tu password.', [{
        text: 'Aceptar',
      }]);
      return;
    }
    this.setState({loading: true});
    const passMd5 = hex_md5(password);
    SumarInversionApi.Login(email, passMd5).then((res)=>{
      console.log('********+ respuesta login *********** ');
      console.log(res);
      if (res.problem === 'NETWORK_ERROR') {
        this.setState({ loading: false });
        Alert.alert('', 'Al parecer existe un problema con tu conexión, intenta más tarde', [{
          text: 'Aceptar',
        }]);
        return;
      }
      if (res.problem === 'CLIENT_ERROR') {
        this.setState({ loading: false });
        Alert.alert('', 'No es posible enviar la solicitud, intenta más tarde', [{
          text: 'Aceptar',
        }]);
        return;
      }
      if (res.problem === 'TIMEOUT_ERROR') {
        this.setState({ loading: false });
        Alert.alert('', 'El servicio tardo demasiado en responder, intenta más tarde', [{
          text: 'Aceptar',
        }]);
        return;
      }
      if (res.problem === 'SERVER_ERROR') {
        this.setState({ loading: false });
        Alert.alert('', 'No se pudo procesar la solicitud, intenta más tarde', [{
          text: 'Aceptar',
        }]);
        return;
      }
      if (res.problem === 'CONNECTION_ERROR') {
        this.setState({ loading: false });
        Alert.alert('', 'Servidor no disponible, intenta más tarde', [{
          text: 'Aceptar',
        }]);
        return;
      }
      else {
        const { usuarios } = res.data;
        if(usuarios.length === 0 ){
          this.setState({loading: false});
          Alert.alert('', 'Datos incorrectos o el usuario no existe, intentalo de nuevo.', [{
            text: 'Aceptar',
          }]);
        }else{
          const usuarioEncontrado = usuarios[0];
          Storage.set('nombre', usuarioEncontrado['nombre'], ()=>{
            Storage.set('apellido', usuarioEncontrado['apellido'], ()=>{
              Storage.set('email', usuarioEncontrado['email'], ()=>{
                Storage.set('userId', usuarioEncontrado['userId'], ()=>{
                  this.setState({loading: false});
                  this.navToHome();
                });
              });
            });
          });
        }
      }
    }).catch((error)=>{
      this.setState({ loading: false });
        console.log('****** error al intentar iniciar sesión *********');
        console.log(error);
        Alert.alert(error, 'No posible iniciar sesión en este momento, intenta más tarde', [{
          text: 'Aceptar',
        }]);
    });
  }

  render() {
    const { loading, email, password } = this.state;
    return (
      <SafeAreaView style={styles.mainContainer}>
         {
          loading ? <WaveIndicator count={1} waveFactor={0.1} color={'rgb(55,109,145)'} size={60}></WaveIndicator> :  
          <ImageBackground style={styles.bgLogin} source={Images.bgLogin}>
            <View style={styles.overlight}></View>
            <View style={styles.mainContainer}>
              <View style={[styles.bodyContent, { justifyContent: 'center' }]}>
                <Image style={styles.logoLogin} source={Images.logoSumarInversion}></Image>
                <TextInput placeholder={'Email...'} value={email} onChangeText={(value) => { this.setState({ email: value }); }}></TextInput>
                <TextInput isPassword={true} placeholder={'Password...'} value={password} onChangeText={(value) => { this.setState({ password: value }); }}></TextInput>
                <Button raised backgroundColor={Colors.colorE} leftIcon={{ name: 'lock' }} title={'Login'.toUpperCase()} onPress={() => { this.LoginExecute() }} />
              </View>
            </View>
          </ImageBackground>
        }
      </SafeAreaView>
    );
  }

}
