import React, { Component } from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import styles from '../theme/styles';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import { Colors } from '../theme/Colors';
import HTMLView from 'react-native-htmlview';
import currencyFormatter from 'currency-formatter';
import moment from 'moment';
import { WaveIndicator } from 'react-native-indicators';

export default class DetalleProyectoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      proyecto: this.props.proyecto,
      porcentajeProyecto: this.props.porcentajeProyecto,
      loading: true
    };
  }

  cleanText(text){
    return text.replace(/<[^>]*>/g, '');
  }

  ObtenerParcialEstimada(proyecto, modulo){
    console.log('************* proyecto *****************');
    console.log(proyecto);
    const date1 = proyecto.fecha;
    const today = new Date();
    const date2 = moment(today).format('YYYY-MM-DD');
    let diff = Math.abs(Date.parse(date2) - Date.parse(date1)).toString().substr(0, 8);
    const years = Math.floor(diff / (365*60*60*24));
    const months = Math.floor((diff - years * 365*60*60*24) / (30*60*60*24));
    const days = Math.floor((diff - years * 365*60*60*24 - months*30*60*60*24)/ (60*60*24));
    const porcentaje_rentaMax = parseInt(proyecto.rentaHasta)/parseInt(proyecto.plazo);
    const porcentaje_rentaMin = parseInt(proyecto.rentaDesde)/parseInt(proyecto.plazo);
    const mesesModulo1 = parseInt(proyecto.plazo) - parseInt(modulo.mes);
    const ganancia_Max1 =  porcentaje_rentaMax * mesesModulo1;
    const ganancia_Min1 = porcentaje_rentaMin * mesesModulo1;
    const gananciaP_Max1 = porcentaje_rentaMax * (((years*12) + months) - parseInt(modulo.mes));
    const gananciaP_Min1 = porcentaje_rentaMin*(((years*12) + months) - parseInt(modulo.mes));
    const rentaFinalEstimadaMAX1 = (parseInt(modulo.inversion) * ganancia_Max1)/100;
    const rentaFinalEstimadaMIN1 = (parseInt(modulo.inversion) * ganancia_Min1)/100;
    const mesesModulo = parseInt(proyecto.plazo) - parseInt(modulo.mes);
    const ganancia_Max = porcentaje_rentaMax * mesesModulo;
    const ganancia_Min = porcentaje_rentaMin * mesesModulo;
    const gananciaP_Max = porcentaje_rentaMax * (((years*12) + months) - parseInt(modulo.mes));
    const gananciaP_Min = porcentaje_rentaMin *(((years*12) + months) - parseInt(modulo.mes));
    const rentaFinalEstimadaMAX = (parseInt(modulo.inversion) * ganancia_Max)/100;
    const rentaFinalEstimadaMIN = (parseInt(modulo.inversion) * ganancia_Min)/100;
    console.log(modulo.inversion);
    const rentaParcialEstimadaMAX =  (parseInt(modulo.inversion) * gananciaP_Max)/100;
    console.log('**** renta parcial estimada *******');
    console.log(rentaParcialEstimadaMAX);
    const rentaParcialEstimadaMIN = (parseInt(modulo.inversion) * gananciaP_Min)/100;
    const pgTotal_rentaFinalEstimadaMAX = parseInt(modulo.inversion) + rentaFinalEstimadaMAX;
    const pgInversion_rentaFinalEstimadaMAX = (parseInt(modulo.inversion) / pgTotal_rentaFinalEstimadaMAX)*100;
    const pgInversion_rentaParcialEstimadaMAX = (rentaParcialEstimadaMAX / pgTotal_rentaFinalEstimadaMAX)*100;
    const pgTotal_rentaFinalEstimadaMIN = parseInt(modulo.mes) + rentaFinalEstimadaMIN;
    const pgInversion_rentaFinalEstimadaMIN = (parseInt(modulo.inversion) / pgTotal_rentaFinalEstimadaMIN)*100;
    const pgInversion_rentaParcialEstimadaMIN = (rentaParcialEstimadaMIN/pgTotal_rentaFinalEstimadaMIN)*100;

    return currencyFormatter.format(rentaParcialEstimadaMAX.toFixed(2), {code: 'USD', symbol: '', thousand: '.', decimal: ','});
  }

  ObtenerFinalEstimada(proyecto, modulo){
    console.log('************* proyecto *****************');
    console.log(proyecto);
    const date1 = proyecto.fecha;
    const today = new Date();
    const date2 = moment(today).format('YYYY-MM-DD');
    let diff = Math.abs(Date.parse(date2) - Date.parse(date1)).toString().substr(0, 8);
    const years = Math.floor(diff / (365*60*60*24));
    const months = Math.floor((diff - years * 365*60*60*24) / (30*60*60*24));
    const days = Math.floor((diff - years * 365*60*60*24 - months*30*60*60*24)/ (60*60*24));
    const porcentaje_rentaMax = parseInt(proyecto.rentaHasta)/parseInt(proyecto.plazo);
    const porcentaje_rentaMin = parseInt(proyecto.rentaDesde)/parseInt(proyecto.plazo);
    const mesesModulo1 = parseInt(proyecto.plazo) - parseInt(modulo.mes);
    const ganancia_Max1 =  porcentaje_rentaMax * mesesModulo1;
    const ganancia_Min1 = porcentaje_rentaMin * mesesModulo1;
    const gananciaP_Max1 = porcentaje_rentaMax * (((years*12) + months) - parseInt(modulo.mes));
    const gananciaP_Min1 = porcentaje_rentaMin*(((years*12) + months) - parseInt(modulo.mes));
    const rentaFinalEstimadaMAX1 = (parseInt(modulo.inversion) * ganancia_Max1)/100;
    const rentaFinalEstimadaMIN1 = (parseInt(modulo.inversion) * ganancia_Min1)/100;
    const mesesModulo = parseInt(proyecto.plazo) - parseInt(modulo.mes);
    const ganancia_Max = porcentaje_rentaMax * mesesModulo;
    const ganancia_Min = porcentaje_rentaMin * mesesModulo;
    const gananciaP_Max = porcentaje_rentaMax * (((years*12) + months) - parseInt(modulo.mes));
    const gananciaP_Min = porcentaje_rentaMin *(((years*12) + months) - parseInt(modulo.mes));
    const rentaFinalEstimadaMAX = (parseInt(modulo.inversion) * ganancia_Max)/100;
    const rentaFinalEstimadaMIN = (parseInt(modulo.inversion) * ganancia_Min)/100;
    console.log(modulo.inversion);
    const rentaParcialEstimadaMAX =  (parseInt(modulo.inversion) * gananciaP_Max)/100;
    console.log('**** renta parcial estimada *******');
    console.log(rentaParcialEstimadaMAX);
    const rentaParcialEstimadaMIN = (parseInt(modulo.inversion) * gananciaP_Min)/100;
    const pgTotal_rentaFinalEstimadaMAX = parseInt(modulo.inversion) + rentaFinalEstimadaMAX;
    const pgInversion_rentaFinalEstimadaMAX = (parseInt(modulo.inversion) / pgTotal_rentaFinalEstimadaMAX)*100;
    const pgInversion_rentaParcialEstimadaMAX = (rentaParcialEstimadaMAX / pgTotal_rentaFinalEstimadaMAX)*100;
    const pgTotal_rentaFinalEstimadaMIN = parseInt(modulo.mes) + rentaFinalEstimadaMIN;
    const pgInversion_rentaFinalEstimadaMIN = (parseInt(modulo.inversion) / pgTotal_rentaFinalEstimadaMIN)*100;
    const pgInversion_rentaParcialEstimadaMIN = (rentaParcialEstimadaMIN/pgTotal_rentaFinalEstimadaMIN)*100;

    return currencyFormatter.format(rentaFinalEstimadaMAX.toFixed(2), {code: 'USD', symbol: '', thousand: '.', decimal: ','});
  }

  ObtenerParcialEstimadaMin(proyecto, modulo){
    console.log('************* proyecto *****************');
    console.log(proyecto);
    const date1 = proyecto.fecha;
    const today = new Date();
    const date2 = moment(today).format('YYYY-MM-DD');
    let diff = Math.abs(Date.parse(date2) - Date.parse(date1)).toString().substr(0, 8);
    const years = Math.floor(diff / (365*60*60*24));
    const months = Math.floor((diff - years * 365*60*60*24) / (30*60*60*24));
    const days = Math.floor((diff - years * 365*60*60*24 - months*30*60*60*24)/ (60*60*24));
    const porcentaje_rentaMax = parseInt(proyecto.rentaHasta)/parseInt(proyecto.plazo);
    const porcentaje_rentaMin = parseInt(proyecto.rentaDesde)/parseInt(proyecto.plazo);
    const mesesModulo1 = parseInt(proyecto.plazo) - parseInt(modulo.mes);
    const ganancia_Max1 =  porcentaje_rentaMax * mesesModulo1;
    const ganancia_Min1 = porcentaje_rentaMin * mesesModulo1;
    const gananciaP_Max1 = porcentaje_rentaMax * (((years*12) + months) - parseInt(modulo.mes));
    const gananciaP_Min1 = porcentaje_rentaMin*(((years*12) + months) - parseInt(modulo.mes));
    const rentaFinalEstimadaMAX1 = (parseInt(modulo.inversion) * ganancia_Max1)/100;
    const rentaFinalEstimadaMIN1 = (parseInt(modulo.inversion) * ganancia_Min1)/100;
    const mesesModulo = parseInt(proyecto.plazo) - parseInt(modulo.mes);
    const ganancia_Max = porcentaje_rentaMax * mesesModulo;
    const ganancia_Min = porcentaje_rentaMin * mesesModulo;
    const gananciaP_Max = porcentaje_rentaMax * (((years*12) + months) - parseInt(modulo.mes));
    const gananciaP_Min = porcentaje_rentaMin *(((years*12) + months) - parseInt(modulo.mes));
    const rentaFinalEstimadaMAX = (parseInt(modulo.inversion) * ganancia_Max)/100;
    const rentaFinalEstimadaMIN = (parseInt(modulo.inversion) * ganancia_Min)/100;
    console.log(modulo.inversion);
    const rentaParcialEstimadaMAX =  (parseInt(modulo.inversion) * gananciaP_Max)/100;
    console.log('**** renta parcial estimada *******');
    console.log(rentaParcialEstimadaMAX);
    const rentaParcialEstimadaMIN = (parseInt(modulo.inversion) * gananciaP_Min)/100;
    const pgTotal_rentaFinalEstimadaMAX = parseInt(modulo.inversion) + rentaFinalEstimadaMAX;
    const pgInversion_rentaFinalEstimadaMAX = (parseInt(modulo.inversion) / pgTotal_rentaFinalEstimadaMAX)*100;
    const pgInversion_rentaParcialEstimadaMAX = (rentaParcialEstimadaMAX / pgTotal_rentaFinalEstimadaMAX)*100;
    const pgTotal_rentaFinalEstimadaMIN = parseInt(modulo.mes) + rentaFinalEstimadaMIN;
    const pgInversion_rentaFinalEstimadaMIN = (parseInt(modulo.inversion) / pgTotal_rentaFinalEstimadaMIN)*100;
    const pgInversion_rentaParcialEstimadaMIN = (rentaParcialEstimadaMIN/pgTotal_rentaFinalEstimadaMIN)*100;

    return currencyFormatter.format(rentaParcialEstimadaMIN.toFixed(2), {code: 'USD', symbol: '', thousand: '.', decimal: ','});
  }

  ObtenerFinalEstimadaMin(proyecto, modulo){
    console.log('************* proyecto *****************');
    console.log(proyecto);
    const date1 = proyecto.fecha;
    const today = new Date();
    const date2 = moment(today).format('YYYY-MM-DD');
    let diff = Math.abs(Date.parse(date2) - Date.parse(date1)).toString().substr(0, 8);
    const years = Math.floor(diff / (365*60*60*24));
    const months = Math.floor((diff - years * 365*60*60*24) / (30*60*60*24));
    const days = Math.floor((diff - years * 365*60*60*24 - months*30*60*60*24)/ (60*60*24));
    const porcentaje_rentaMax = parseInt(proyecto.rentaHasta)/parseInt(proyecto.plazo);
    const porcentaje_rentaMin = parseInt(proyecto.rentaDesde)/parseInt(proyecto.plazo);
    const mesesModulo1 = parseInt(proyecto.plazo) - parseInt(modulo.mes);
    const ganancia_Max1 =  porcentaje_rentaMax * mesesModulo1;
    const ganancia_Min1 = porcentaje_rentaMin * mesesModulo1;
    const gananciaP_Max1 = porcentaje_rentaMax * (((years*12) + months) - parseInt(modulo.mes));
    const gananciaP_Min1 = porcentaje_rentaMin*(((years*12) + months) - parseInt(modulo.mes));
    const rentaFinalEstimadaMAX1 = (parseInt(modulo.inversion) * ganancia_Max1)/100;
    const rentaFinalEstimadaMIN1 = (parseInt(modulo.inversion) * ganancia_Min1)/100;
    const mesesModulo = parseInt(proyecto.plazo) - parseInt(modulo.mes);
    const ganancia_Max = porcentaje_rentaMax * mesesModulo;
    const ganancia_Min = porcentaje_rentaMin * mesesModulo;
    const gananciaP_Max = porcentaje_rentaMax * (((years*12) + months) - parseInt(modulo.mes));
    const gananciaP_Min = porcentaje_rentaMin *(((years*12) + months) - parseInt(modulo.mes));
    const rentaFinalEstimadaMAX = (parseInt(modulo.inversion) * ganancia_Max)/100;
    const rentaFinalEstimadaMIN = (parseInt(modulo.inversion) * ganancia_Min)/100;
    console.log(modulo.inversion);
    const rentaParcialEstimadaMAX =  (parseInt(modulo.inversion) * gananciaP_Max)/100;
    console.log('**** renta parcial estimada *******');
    console.log(rentaParcialEstimadaMAX);
    const rentaParcialEstimadaMIN = (parseInt(modulo.inversion) * gananciaP_Min)/100;
    const pgTotal_rentaFinalEstimadaMAX = parseInt(modulo.inversion) + rentaFinalEstimadaMAX;
    const pgInversion_rentaFinalEstimadaMAX = (parseInt(modulo.inversion) / pgTotal_rentaFinalEstimadaMAX)*100;
    const pgInversion_rentaParcialEstimadaMAX = (rentaParcialEstimadaMAX / pgTotal_rentaFinalEstimadaMAX)*100;
    const pgTotal_rentaFinalEstimadaMIN = parseInt(modulo.mes) + rentaFinalEstimadaMIN;
    const pgInversion_rentaFinalEstimadaMIN = (parseInt(modulo.inversion) / pgTotal_rentaFinalEstimadaMIN)*100;
    const pgInversion_rentaParcialEstimadaMIN = (rentaParcialEstimadaMIN/pgTotal_rentaFinalEstimadaMIN)*100;

    return currencyFormatter.format(rentaFinalEstimadaMIN.toFixed(2), {code: 'USD', symbol: '', thousand: '.', decimal: ','});
  }

  render() {
    const { proyecto, porcentajeProyecto, loading } = this.state;
    let moduloNum = 1;
    return (
      <ScrollView style={styles.mainContainer} showsVerticalScrollIndicator={false}>
        {
          loading ? <WaveIndicator count={1} waveFactor={0.1} color={'rgb(55,109,145)'} size={60}></WaveIndicator> :
          <View>
            <View style={styles.imageDetailHolder}>
              <Image resizeMethod={'resize'} style={styles.imageDetail} source={{uri: proyecto.image}}></Image>
              <View style={styles.txtModules}>
                <View style={styles.txtModulesHolder}>
                  <Text style={{color: Colors.blanco, fontSize: 14}}>Modulos adquiridos: {proyecto.modulosAdquiridos.length}</Text>
                </View>
              </View>
            </View>
            <View style={styles.bodyContent}>
              <View style={styles.progressHolder}>
                <Text style={[{marginBottom: 10, color: Colors.gris, fontSize: 18}]}>{porcentajeProyecto}%</Text>
                <ProgressBarAnimated borderWidth={0} borderRadius={0} height={5} backgroundColor={Colors.dark} width={160} value={porcentajeProyecto}/>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 10}}>
                <Text style={[styles.customFontDDDINBold]}>Inicio: </Text>
                <Text>{moment(proyecto.fecha).format('DD, YYYY')}</Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'center',}}>
                <Text style={styles.customFontDDDINBold}>Plazo:</Text>
                <Text> {proyecto.plazo} meses</Text>
              </View>
              <View style={styles.dataProyectHolder}>
                {
                  proyecto.modulosAdquiridos.map((item, key)=>{
                    return(
                      <View style={styles.borderer1}>
                        <View style={{padding: 5, justifyContent: 'center', flexDirection: 'row'}}>
                          <Text style={[styles.customFontDDDINBold, {fontSize: 18}]}>Módulo {moduloNum++} -</Text>
                          <Text style={{fontSize: 18}}> Mes Ingreso {item.mes}</Text>
                        </View>
                        <View style={{justifyContent: 'center', flexDirection: 'row'}}>
                          <Text style={[styles.customFontDDDINBold]}>inversión: USD </Text>
                          <Text>{currencyFormatter.format(item.inversion, {code: 'USD', symbol: '', thousand: '.', decimal: ','})}</Text>
                        </View>
                        <View style={styles.line}></View>
                        <View style={{padding: 15}}>
                          <Text style={[styles.tituloDetail, {color: Colors.negro, fontSize: 16}]}>{'renta estimada '.toUpperCase()+proyecto.rentaHasta}%</Text>
                          <Text style={{color: Colors.verde, fontSize: 16}}>Parcial Estimada: USD {this.ObtenerParcialEstimada(proyecto, item)} <Text style={{color: Colors.negro}}>|</Text></Text>
                          <Text style={{color: Colors.colorC, fontSize: 16}}>Final Estimada: USD {this.ObtenerFinalEstimada(proyecto, item)}</Text>
                          <View style={[styles.line, {marginVertical: 10}]}></View>
                          <Text style={[styles.tituloDetail, {color: Colors.negro, fontSize: 16}]}>{'renta estimada '.toUpperCase()+proyecto.rentaDesde}%</Text>
                          <Text style={{color: Colors.verde, fontSize: 16}}>Parcial Estimada: USD {this.ObtenerParcialEstimadaMin(proyecto, item)} <Text style={{color: Colors.negro}}>|</Text></Text>
                          <Text style={{color: Colors.colorC, fontSize: 16}}>Final Estimada: USD {this.ObtenerFinalEstimadaMin(proyecto, item)}</Text>
                        </View>
                      </View>
                    )
                  })
                }
              </View>
            </View>
          </View>
        }
      </ScrollView>
    );
  }

  componentDidMount(){
    setTimeout(()=>{
      this.setState({loading: false})
    }, 1000);
  }
}
