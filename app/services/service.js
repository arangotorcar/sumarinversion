import apisauce from 'apisauce';
import config from '../config';
import { JsonToForm } from '../transforms/JsonToFormTransform';

const create = () => {

    const api = apisauce.create({
        baseURL: config.api.rest,
        timeout: 5000,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept': 'application/json',
        }
    });

    const Login = (email, pdwUser) => api.post('/login', JsonToForm({ 
        email: email,
        contrasenia: pdwUser
        })
    );

    const GetProyectosDestacados = () => api.get('/proyectos_destacados');

    const GetMisInversiones = (idUser) => api.post('/mis_inversiones', JsonToForm({
        idUser: idUser
    }));

    return { 
        Login,
        GetProyectosDestacados,
        GetMisInversiones
    };
}   

export default { create };