export default class Helpers {

    static IsEmailValid(email){
        const emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        console.log('******** validación email *********');
        console.log(emailRegex.test(email));
        return emailRegex.test(email);
    }

}