import { AsyncStorage } from 'react-native';

const Storage = {
  get: (key, callback) => {
    AsyncStorage.getItem(key)
    .then((value) => {
      const valueJson = JSON.parse(value);

      callback(valueJson);
    })
    .catch((error) => {
      callback(null);
    })
  },
  set: (key, value, callback) => {
    try {
      const valueString = JSON.stringify(value);

      if (callback) {
        AsyncStorage.setItem(key, valueString, callback);
      } else {
        AsyncStorage.setItem(key, valueString);
      }
    } catch (error) {
      console.log(error);
    }
  }
}

export { Storage };
