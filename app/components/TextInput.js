import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { FormInput } from 'react-native-elements'
import styles from '../theme/styles';
import { Colors } from '../theme/Colors';

export default class TextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const props = this.props;
    return (
      <View style={styles.textInputHolder}>
        <FormInput containerStyle={styles.textInputContainerStyle} autoCapitalize={"none"} secureTextEntry={props.isPassword} multiline={props.multiline} placeholder={props.placeholder} value={props.value} onChangeText={props.onChangeText}/>
      </View>
    );
  }
}
